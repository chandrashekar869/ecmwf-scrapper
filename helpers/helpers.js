//Pick config data from /config/default.json
var config = require("config");
var moment = require('moment');


//Generate the file name in the format z_cams_c_ecmf_yyyymmddhhmmss_vvvv_tt_lt_sss_param.grib
function genFileName(param, directoryName) {
    var fileName = "";
    var fileOrder = [config.fileNamePrefix, directoryName + "0000", config.fileNameSuffix, param, config.fileFormat];
    fileName = fileOrder.join("");
    return fileName;
}

//Returns the latest directory name based on the latest date
function getLatestDirectory(list) {
    var dirNamesDates = list.map(dir => moment(dir.name, 'YYYYMMDDHH'));
    return moment.max(dirNamesDates).format("YYYYMMDDHH");
}

module.exports = { genFileName, getLatestDirectory };
