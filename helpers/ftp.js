var config = require('config');
var Client = require('ftp');
var fs = require('fs');
var async = require('async');

var helpers = require('./helpers');

var c = new Client();

function connect() {
  c.connect(config.ftpConfig);
}

function disconnect() {
  c.end();
}

function ready(callback) {
  c.on('ready', function () {
    callback();
  });
}

function listFiles(path) {
  return new Promise((res, rej) => {
    ready(() => {
      c.list([path], function (err, list) {
        if (err)
          throw err;
        else {
          if (list.length > 0)
            res(list);
          else
            rej(null);
        }
      });
    })
  });
}

function downloadFiles(dirName) {
  async.map(config.paramsToDownload,
    (param, callback) => {
      var fileName = helpers.genFileName(param, dirName);
      var filePath = config.directoryLocation + "/" + dirName + "/" + fileName;
      console.log(filePath);
      ready(() => {
        c.get([filePath], function (err, stream) {
          if (err) throw err;
          stream.once('close', function () { c.end(); });
          stream.pipe(fs.createWriteStream(fileName));
          stream.on('end', function () {
            // This may not been called since we are destroying the stream
            // the first time 'data' event is received
            console.log('All the data in the file has been read');
            callback();
          });
        });
      });
    },
    (err, results) => {
      if (err)
        throw err;
      else
        disconnect();
      c.destroy();
    });
}

module.exports = { connect, listFiles, downloadFiles }
