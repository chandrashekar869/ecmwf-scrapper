var config = require('config');

var ftp = require('./helpers/ftp');
var helpers = require('./helpers/helpers');

var latestDirectory = "";

ftp.connect();
ftp.listFiles(config.directoryLocation)
    .then(dir => {
        ftp.connect();
        latestDirectory = helpers.getLatestDirectory(dir);
        ftp.downloadFiles(latestDirectory);
    })
    .catch(err => {
        console.log(err);
    });
// helpers.getLatestDirectory(ftp.listFiles(config.directoryLocation));